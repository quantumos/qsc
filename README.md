# QSC: The Quantum Software Center # # #

The Quantum Software Center is an easy to use, cross-arch-platform software installer based on python, pygtk, and pyalpm.

## Why QSC? ##
There are plenty of reasons to use QSC:
- Works out of the box
- No dependencies on PackageKit
- Not tied to GNOME or QT

We designed the Quantum Software Center to be a lightweight, easy to use software center that just works.
Taking inspiration from the old Ubuntu 12.x era Software Center and MacOS's App Store, we've created
a GUI software center for Arch Linux systems that only requires GTK - which is preinstalled by many
desktop environments.

### Why not another software center? ###
Other software center GUIs are listed in the [Arch Wiki](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Graphical),
but none of them fit our needs. All of the GUIs listed in the wiki were either way too complicated or
had tricky dependency problems (Apper and Discover depend on KDE Plasma in some way, and GNOME Software
needs GNOME to work). Our solution was to develop a pure GTK application without dependency issues.

### What's wrong with PackageKit? ###
TL;DR: [PackageKit is insecure by default](https://bugs.archlinux.org/task/50459). It's also missing some support
for Arch (namely AUR packages), so we opted to write our own backend instead.

# Dev Info #

## Getting Started ##
To work on QSC, it's recommended that you have Python 3.5+ and Arch Linux
or an Arch Linux derivative.

1. Clone the repo.
2. Run `./setup.sh`. This should install all the dependencies.
3. Run `./install.sh`. This should install the necessary files for QSC to work.
4. Start the qscd service with `sudo python3 -m qscd`.
5. Have fun!

## How is QSC structured? ##
QSC is comprised of 2 parts: the GTK frontend (qsc) and the backend service (qscd).

qscd is a D-Bus service with root privileges. It handles interaction with the actual
package manager (pacman) and authenticates using polkit, which allows fine control
over permissions. All methods are exported via the `com.quantumos.QSCDInterface/QSCD` object.
All qscd object methods are wrapped in a Python API. For documentation on this API,
see `qscd/api.py`.

qsc is a pure PyGTK frontend to qscd. It connects to qscd via the API and provides an easy
to use, graphical interface for users to to manage packages.

## The Stack ##
QSC is written in PyGTK (Python 3) and uses various GLib methods via `python-gobject`.
QSCD uses `dbus-python` for D-Bus publishing and `pyalpm` for interaction with the
`pacman` package manager.
