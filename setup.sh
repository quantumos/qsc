#!/bin/bash

# Get deps
pacman -Sy
pacman -S pygobject-devel
pacman -S pyalpm

# Upgrade libalpm if needed
pacman -S pacman

# Install some libs
pacman -S python-dbus
pip3 install dill
