# How the DBus Service works #
QSCD is a DBus service using polkit as an auth backend. Here's what that means practically:

## Adding a method to the DBus service ##
Adding a method is simple. First, you need a decorator and your function parameters:
```python
@dbus.service.method("com.quantumos.QSCDInterface",
                     in_signature="s", out_signature="as",
                     sender_keyword="sender", connection_keyword="conn")
def YourMethod(self, param, sender=None, conn=None):
    return ["your", "output"]
```
Let's start with the decorator. The parts you care about are the in and out signatures.
These are strings representing your parameters (refer to [here](https://dbus.freedesktop.org/doc/dbus-specification.html#type-system) for full docs on the strings). For example, here, we're taking an input of one string (`param` in our method) and returning an output of an array of strings (`as` = array of strings). You can have as many input parameters as you want.

As long as your input parameters and return values match the out_signature, you're free to do anything you want within the function.

## Checking auth ##
To check for authorization, just use:
```python
self._check_polkit_privilege(sender, conn, "com.quantumos.qscdservice.auth")
```
