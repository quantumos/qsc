# qscd: the Quantum Software Center Daemon #
qscd is a D-Bus service which handles installing, removing, and fetching info on packages. Its main use is as a backend to QSC.

## Why a separate daemon? ##
QSC requires sudo (root) privileges in order to install applications. Unfortunately, escalating a program to root privileges
in the middle of its execution is messy no matter which way you do it.

QSC solves this problem by adding a D-Bus polkit authenticated service (qscd) which runs with root privileges.
qscd handles and authenticates commands (such as installing an app) from QSC. This architecture is much more secure
and promotes code modularity.

