#!/usr/bin/env python3
#
# pycman-remove - A Python implementation of Pacman
# Copyright (C) 2011 Rémy Oudompheng <remy@archlinux.org>
# Edited and adapted for use in qscd - 2019 Vincent Wang
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""
A Python implementation of pacman -R

This script uninstalls packages. Various options control
the effect on dependencies of/on given targets.
"""

import sys
from qscd.package_mgmt.pypac import config, transaction

def remove(handle, pkgs):
	# prepare target list
	db = handle.get_localdb()
	targets = []
	for name in pkgs:
		pkg = db.get_pkg(name)
		if pkg is None:
			print("error: '%s': target not found" % name)
			return 1
		targets.append(pkg)

	t = transaction.init(handle)

	for pkg in targets:
		t.remove_pkg(pkg)

	ok = transaction.finalize(t)
	return (0 if ok else 1)
