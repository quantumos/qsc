#!/usr/bin/env python3
#
# pycman-query - A Python implementation of Pacman
# Copyright (C) 2011 Rémy Oudompheng <remy@archlinux.org>
# Edited and adapted for use in qscd - 2019 Vincent Wang
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""
A Python implementation of pacman -Q

This script displays information about installed packages.
"""

import os
import sys

import pyalpm
from qscd.package_mgmt.pypac import config
from qscd.package_mgmt.pypac import pkginfo
from enum import Enum

class filters(Enum):
	FOREIGN = 0
	DEPS = 1
	EXPLICIT = 2
	UNREQUIRED = 3
	UPGRADES = 4


def filter_pkglist(handle, pkglist, filtertype):
	result = []
	if filtertype == filters.FOREIGN:
		syncpkgs = set()
		for db in handle.get_syncdbs():
			syncpkgs |= set(p.name for p in db.pkgcache)
	for pkg in pkglist:
		if filtertype == filters.DEPS and pkg.reason == pyalpm.PKG_REASON_EXPLICIT:
			continue
		if filtertype == filters.EXPLICIT and pkg.reason == pyalpm.PKG_REASON_DEPEND:
			continue
		if filtertype == filters.UNREQUIRED and len(pkg.compute_requiredby()) > 0:
			continue
		if filtertype == filters.FOREIGN and pkg.name in syncpkgs:
			continue
		if filtertype == filters.UPGRADES and pyalpm.sync_newversion(pkg, handle.get_syncdbs()) is None:
			continue
		result.append(pkg)
	return result

def display_pkg(pkg, options):
	displaystyle = 'file' if options.package else 'local'
	if options.info > 0:
		pkginfo.display_pkginfo(pkg, level=options.info, style=displaystyle)
	elif not options.listfiles:
		if options.quiet:
			print(pkg.name)
		else:
			print(pkg.name, pkg.version)

	if options.listfiles:
		if options.quiet:
			[print('/' + path) for path, size, mode in pkg.files]
		else:
			[print(pkg.name, '/' + path) for path, size, mode in pkg.files]

def find_file(handle, filenames, options):
	"lookup for files in local packages"
	ret = 0
	if len(filenames) == 0:
		print("error: no targets specified")
		ret = 1

	localpkgs = handle.get_localdb().pkgcache
	n_pkg = len(localpkgs)
	filelists = [None] * n_pkg

	for name in filenames:
		lookupname = None
		if not os.path.isabs(name):
			# lookup in PATH
			for dirname in os.getenv('PATH').split(':'):
				if os.path.lexists(os.path.join(dirname, name)):
					name = os.path.join(dirname, name)
					lookupname = name
			if lookupname is None:
				print("error: failed to find '%s' in PATH: No such file or directory" % name)
				ret = 1
				continue
		else:
			if not os.path.lexists(name):
				print("error: failed to read file '%s': No such file or directory" % name)
				ret = 1
				continue
			lookupname = name
		lookupname = os.path.normpath(lookupname)
		lookupname = lookupname.lstrip('/')
		found = False
		for i, pkg, files in zip(range(n_pkg), localpkgs, filelists):
			if files is None:
				files = pkg.files
				filelists[i] = files

			if lookupname in files:
				if options.quiet:
					print(pkg.name)
				else:
					print(pkg.name, "is owned by", pkg.name, pkg.version)
				found = True
				break

		if not found:
			print('error: no package owns', name)
			ret = 1

	return ret

def find_search(handle, patterns, options):
	db = handle.get_localdb()
	results = db.search(*patterns)
	if len(results) == 0:
		return 1
	for pkg in results:
		if options.quiet:
			print(pkg.name)
		else:
			print("%s/%s %s" % (pkg.db.name, pkg.name, pkg.version))
			print("    " + pkg.desc)
	return 0

