#!/usr/bin/env python3
#
# pycman-sync - A Python implementation of Pacman
# Copyright (C) 2011 Rémy Oudompheng <remy@archlinux.org>
# Edited and adapted for use in qscd - 2019 Vincent Wang
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""
A Python implementation of pacman -S

This script displays information about packages available in repositories,
and is also used to install/upgrade/remove them.
"""

import sys

import pyalpm
from qscd.package_mgmt.pypac import config
from qscd.package_mgmt.pypac import pkginfo
from qscd.package_mgmt.pypac import transaction

def do_clean(options=None):
	raise NotImplementedError

def do_refresh(handle):
	"Sync databases like pacman -Sy"
	force = False
	for db in handle.get_syncdbs():
		t = transaction.init(handle)
		db.update(force)
		t.release()
	return 0

def do_sysupgrade(handle, options=None, downgrade=False):
	"Upgrade a system like pacman -Su"
	t = transaction.init(handle, options)
	t.sysupgrade(downgrade)
	if len(t.to_add) + len(t.to_remove) == 0:
		print("nothing to do")
		t.release()
		return 0
	else:
		ok = transaction.finalize(t)
		return (0 if ok else 1)

def do_install(handle, pkgs, options=None):
	"Install a list of packages like pacman -S"
	repos = dict((db.name, db) for db in handle.get_syncdbs())
	if len(pkgs) == 0:
		print("error: no targets specified")
		return 1

	targets = []
	for name in pkgs:
		ok, pkg = find_sync_package(handle, name, repos)
		if not ok:
			print('error:', pkg)
			return 1
		else:
			targets.append(pkg)
	t = transaction.init(handle, options)
	[t.add_pkg(pkg) for pkg in targets]
	ok = transaction.finalize(t)
	return (0 if ok else 1)

def find_sync_package(handle, pkgname, syncdbs):
	"Finds a package name of the form 'repo/pkgname' or 'pkgname' in a list of DBs"
	if '/' in pkgname:
		repo, pkgname = pkgname.split('/', 1)
		db = syncdbs.get(repo)
		if db is None:
			return False, "repository '%s' does not exist" % repo
		pkg = db.get_pkg(pkgname)
		if pkg is None:
			return False, "package '%s' was not found in repository '%s'" % (pkgname, repo)
		return True, pkg
	else:
		for db in syncdbs.values():
			pkg = db.get_pkg(pkgname)
			if pkg is not None:
				return True, pkg
		return False, "package '%s' was not found" % pkgname

# Query actions

# def show_groups(handle, ):
# 	"Show groups like pacman -Sg"
# 	for repo in handle.get_syncdbs():
# 		if len(args.args) == 0:
# 			# list all available groups
# 			[print(name) for name, pkgs in repo.grpcache]
# 		else:
# 			# only print chosen groups
# 			for group in args.args:
# 				grp = repo.read_grp(group)
# 				if grp is None:
# 					continue
# 				else:
# 					name, pkgs = grp
# 				if args.quiet:
# 					[print(pkg.name) for pkg in pkgs]
# 				else:
# 					[print(name, pkg.name) for pkg in pkgs]
# 	return 0

# def show_repo(args):
# 	"Show repository's list of packages like pacman -Sl"
# 	repos = handle.get_syncdbs()
# 	if len(args.args) > 0:
# 		repo_dict = dict((repo.name, repo) for repo in repos)
# 		try:
# 			repos = [repo_dict[name] for name in args.args]
# 		except KeyError as err:
# 			print("error: repository '%s' was not found" % err.args)
# 			return 1
#
# 	for repo in repos:
# 		if args.quiet:
# 			[print(pkg.name) for pkg in repo.pkgcache]
# 		else:
# 			[print(repo.name, pkg.name, pkg.version) for pkg in repo.pkgcache]
# 	return 0

# def show_packages(args):
# 	"Show information about packages like pacman -Si"
# 	retcode = 0
# 	if len(args.args) == 0:
# 		for repo in handle.get_syncdbs():
# 			for pkg in repo.pkgcache:
# 				pkginfo.display_pkginfo(pkg, level=args.info, style='sync')
# 	else:
# 		repos = dict((db.name, db) for db in handle.get_syncdbs())
# 		for pkgname in args.args:
# 			ok, value = find_sync_package(pkgname, repos)
# 			if ok:
# 				pkginfo.display_pkginfo(value, level=args.info, style='sync')
# 			else:
# 				retcode = 1
# 				print("error:", value)
# 	return retcode

def show_search(handle, patterns, options=None, quiet=False):
	results = []
	for db in handle.get_syncdbs():
		results += db.search(*patterns)
	if len(results) == 0:
		return 1
	for pkg in results:
            print("%s/%s %s" % (pkg.db.name, pkg.name, pkg.version))
            print("    " + pkg.desc)
	return results

if __name__ == "__main__":
    handle = config.init_with_config("/etc/pacman.conf")
    do_install(["npm"], handle)
