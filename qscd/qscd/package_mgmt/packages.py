from qscd.package_mgmt.pypac import config, action_sync, action_query, action_remove, transaction
from qscd.enums import OpTypes
from qscd.errors import DBLockError

import pyalpm
import json
import types
import threading
import queue

class ALPMManager:
    def __init__(self, config_file, dbus_service=None, *args, **kwargs):
        self.handle = config.init_with_config(config_file)
        self.operation_queue = queue.Queue() # Queue locking (install/remove) ops
        self.dbus_service = dbus_service # because we gotta get exceptions to client

        # Events
        self.handle.dlcb = kwargs.get("cb_dl", transaction.cb_dl)
        self.handle.eventcb = kwargs.get("cb_event", transaction.cb_event)
        self.handle.questioncb = kwargs.get("cb_conv", transaction.cb_conv)
        self.handle.progresscb = kwargs.get("cb_progress", transaction.cb_progress)

        # For tracking operations
        self.current_op = None
        self.operation_worker = threading.Thread(target=self.process_op_queue)
        self.operation_worker.start()

    def serialize_package(self, package):
        """
        Serialize an alpm.Package to JSON.
        """
        result = {}
        for attr in dir(package):
            if attr[0] == "_":
                continue
            try:
                json.dumps(getattr(package, attr))
            except TypeError:
                continue

            result[attr] = getattr(package, attr)
        print(type(result))
        return json.dumps(result)

    def refresh(self):
        """
        Refreshes and gets latest package lists (e.g. pacman -Sy).
        """
        try:
            for db in self.handle.get_syncdbs():
                db.update(False)
        except Exception as e:
            return 1
        return 0

    def list_upgrades(self):
        """
        Get all currently upgradable packages.
        """
        pkglist = self.handle.get_localdb().pkgcache
        return [self.serialize_package(p) for p in action_query.filter_pkglist(self.handle, pkglist, action_query.filters.UPGRADES)]

    def search(self, search_keyword):
        """
        Search for a package with keyword search_keyword.
        Returns:
            A list of packages in JSON format.
        """
        return [self.serialize_package(p) for p in action_sync.show_search(self.handle, search_keyword)]

    def remove(self, package_name):
        print("Adding package to remove queue...")
        self.operation_queue.put_nowait({"optype": OpTypes.REMOVE, "pkg": package_name})

    def install_or_upgrade(self, package_name):
        repos = dict((db.name, db) for db in self.handle.get_syncdbs())
        ok, pkg = action_sync.find_sync_package(self.handle, package_name, repos)
        print("verifying package:")
        if not ok:
            print("not a valid package")
            return 1
        print("valid package! appending to install queue...")
        self.operation_queue.put_nowait({"optype": OpTypes.INSTALL, "pkg": package_name})
        return 0

    def process_op_queue(self):
        while True:
            try:
                op = self.operation_queue.get()
                print("Processing op", op)
                if op["optype"] == OpTypes.INSTALL:
                    self.current_op = OpTypes.INSTALL
                    action_sync.do_install(self.handle, [op["pkg"]])
                elif op["optype"] == OpTypes.REMOVE:
                    self.current_op = OpTypes.REMOVE
                    action_remove.remove(self.handle, [op["pkg"]])
                else:
                    print("Invalid Optype: ", op)

                self.current_op = None
                self.operation_queue.task_done()
            except pyalpm.error as e:
                print(e, e.args)
                if e.args[1] == 10:
                    # DBLockError
                    # Haven't actually figured out how to properly do this yet
                    pass
                else:
                    raise e


if __name__ == "__main__":
    alpm = ALPMManager("/etc/pacman.conf")
