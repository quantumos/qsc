import dbus
import time
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib
from enums import OpTypes

class QSCDHandler:
    def __init__(self):
        DBusGMainLoop(set_as_default=True)
        self.bus = dbus.SystemBus()
        self.qscd_object = self.bus.get_object("com.quantumos.QSCDService", "/QSCD")
        self.add_progress_listener(DefaultListeners.progress_listener)
        self.add_dl_listener(DefaultListeners.download_listener)

    def upgrade_or_install(self, pkg):
        self.qscd_object.Install(pkg)

    def remove(self, pkg):
        self.qscd_object.Remove(pkg)

    def list_upgrades(self):
        """
        Lists available package upgrades.

        Returns a list of JSON objects representing Packages.
        For a list of attributes in the objects, refer to pyalpm docs.
        NOTE: Functions/methods not included in JSON objects.
        """
        return self.qscd_object.ListUpgradable()

    def search(self, keyword):
        return self.qscd_object.Search(keyword)

    def add_progress_listener(self, listener):
        """
        Add a callback function as a listener for progress events.
        Callback function must have these arguments:

        example_callback(target: str, optype: int, percent: int)
            target: name of package being upgraded/installed
            optype: an int representing an operation (e.g. INSTALL or REMOVE) as specified by OpTypes enum
            percent: progress, in percent
        """
        self.qscd_object.connect_to_signal("ProgressEvent",
                                           listener,
                                           dbus_interface="com.quantumos.QSCDInterface.Events")

    def add_dl_listener(self, listener):
        """
        Add a callback function as a listener for download events.
        Callback function must have these arguments:
        example_callback(filename, percent, tx, total)
        """
        self.qscd_object.connect_to_signal("DownloadEvent",
                                           listener,
                                           dbus_interface="com.quantumos.QSCDInterface.Events")


class DefaultListeners:
    """
    Default listeners for events.
    Testing only; in reality you would use your own listeners.
    """

    @staticmethod
    def progress(target, optype, percent):
        if OpTypes(optype) == OpTypes.INSTALL:
            print(target, "install progress: ", percent)
        elif OpTypes(optype) == OpTypes.REMOVE:
            print(target, "remove progress: ", percent)

    @staticmethod
    def download(filename, percent, tx, total):
        print(target, "download progress: {} ({}/{})".format(round(tx/total), tx, total))

    @staticmethod
    def event(target, percent):
        print(target, "install progress: ", percent)

    @staticmethod
    def conv(target, percent):
        print(target, "install progress: ", percent)

    progress_listener = progress
    download_listener = download
    event_listener = download
    conv_listener = download


if __name__ == "__main__":
    mainloop = GLib.MainLoop()

    receiver = QSCDHandler()

    mainloop.run()
