from qscd.package_mgmt.packages import ALPMManager
from qscd.errors import PermissionDeniedByPolicy, DBLockError, _DBLockError

from gi.repository import GLib
import dbus
import dbus.service
import dbus.mainloop.glib


class QSCD(dbus.service.Object):
    def __init__(self, conn=None, object_path=None, bus_name=None):
        dbus.service.Object.__init__(self, conn, object_path, bus_name)

        # Variables used by _check_polkit_privilege
        self.dbus_info = None
        self.polkit = None
        self.enforce_polkit = True
        self.alpm_manager = ALPMManager("/etc/pacman.conf",
                                        self,
                                        cb_progress=self._progress_event,
                                        cb_dl=self._download_event)

    def _check_polkit_privilege(self, sender, conn, privilege):
        """
        Check that the sender has sufficient polkit privilege.
        Sender = sender's D-Bus name (something like ':1:322')

        Docs for this function are in ../docs/SERVICE.md

        Returns if caller has privileges, else raises PermissionDeniedByPolicy.
        """
        if sender is None and conn is None:
            # Not called thru D-bus
            print("Local call, ignoring polkit!")
            return
        if not self.enforce_polkit:
            # Polkit permission enforcement off, just do it
            print("Polkit enforcement off, ignoring request for enforcement!")
            return

        # Get Peer PID
        if self.dbus_info is None:
            # Get DBus Interface and get info thru that
            self.dbus_info = dbus.Interface(conn.get_object("org.freedesktop.DBus",
                                                            "/org/freedesktop/DBus/Bus", False),
                                            "org.freedesktop.DBus")
        pid = self.dbus_info.GetConnectionUnixProcessID(sender)

        # Query polkit
        if self.polkit is None:
            self.polkit = dbus.Interface(dbus.SystemBus().get_object(
            "org.freedesktop.PolicyKit1",
            "/org/freedesktop/PolicyKit1/Authority", False),
                                         "org.freedesktop.PolicyKit1.Authority")

        # Check auth against polkit; if it times out, try again
        try:
            auth_response = self.polkit.CheckAuthorization(
                ("unix-process", {"pid": dbus.UInt32(pid, variant_level=1),
                                  "start-time": dbus.UInt64(0, variant_level=1)}),
                privilege, {"AllowUserInteraction": "true"}, dbus.UInt32(1), "", timeout=600)
            print(auth_response)
            (is_auth, _, details) = auth_response
        except dbus.DBusException as e:
            if e._dbus_error_name == "org.freedesktop.DBus.Error.ServiceUnknown":
                # polkitd timeout, retry
                self.polkit = None
                return self._check_polkit_privilege(sender, conn, privilege)
            else:
                # it's another error, propagate it
                raise

        if not is_auth:
            # Aww, not authorized :(
            print(":(")
            raise PermissionDeniedByPolicy(privilege)

        print("Successful authorization!")

    # --------------------------------------------------------------
    # Methods
    @dbus.service.method("com.quantumos.QSCDInterface",
                         in_signature="s", out_signature="q",
                         sender_keyword="sender", connection_keyword="conn")
    def Install(self, package, sender=None, conn=None):
        self._check_polkit_privilege(sender, conn, "com.quantumos.qscdservice.auth")
        try:
            self.alpm_manager.refresh()
            self.alpm_manager.install_or_upgrade(package)
        except _DBLockError:
            raise DBLockError
        return 0

    @dbus.service.method("com.quantumos.QSCDInterface",
                         in_signature="s", out_signature="q",
                         sender_keyword="sender", connection_keyword="conn")
    def Remove(self, package, sender=None, conn=None):
        self._check_polkit_privilege(sender, conn, "com.quantumos.qscdservice.auth")
        try:
            self.alpm_manager.remove(package)
        except _DBLockError:
            raise DBLockError
        return 0

    @dbus.service.method("com.quantumos.QSCDInterface",
                         in_signature="", out_signature="as",
                         sender_keyword="sender", connection_keyword="conn")
    def ListUpgradable(self, sender=None, conn=None):
        return self.alpm_manager.list_upgrades()

    @dbus.service.method("com.quantumos.QSCDInterface",
                         in_signature="s", out_signature="as",
                         sender_keyword="sender", connection_keyword="conn")
    def Search(self, keyword, sender=None, conn=None):
        return self.alpm_manager.search(keyword)

    # Events (progress, etc)
    @dbus.service.signal(dbus_interface="com.quantumos.QSCDInterface.Events",
                         signature="sii")
    def ProgressEvent(self, target, optype, percent):
        """
        Emits a ProgressEvent signal to com.quantumos.QSCDInterface.Events when called.
        """
        # Progress event handling should happen on client side
        print("progress event")

    def _progress_event(self,target, percent, n, i):
        """
        Handles calling ProgressEvent - changes some args around
        """
        if target != "":
            self.ProgressEvent(target, self.alpm_manager.current_op.value, percent)

    @dbus.service.signal(dbus_interface="com.quantumos.QSCDInterface.Events",
                         signature="sii")
    def DownloadEvent(self, filename, tx, total):
        # Progress event handling should happen on client side
        pass

    def _download_event(self, filename, tx, total):
        print("download:", filename, tx, total)
        self.DownloadEvent(filename, tx, total)


if __name__ == "__main__":
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SystemBus()
    name = dbus.service.BusName("com.quantumos.QSCDService", bus)
    object = QSCD(bus, "/QSCD")

    mainloop = GLib.MainLoop()
    print("running QSCD...")
    mainloop.run()
