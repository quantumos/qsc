from enum import Enum

class OpTypes(Enum):
    INSTALL = 0
    REMOVE = 1
