from gi.repository import GLib
from qscd.service import QSCD, PermissionDeniedByPolicy

import dbus
import dbus.service
import dbus.mainloop.glib

if __name__ == "__main__":
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SystemBus()
    name = dbus.service.BusName("com.quantumos.QSCDService", bus)
    object = QSCD(bus, "/QSCD")

    mainloop = GLib.MainLoop()
    print("running QSCD...")
    mainloop.run()
