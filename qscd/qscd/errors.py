import dbus

class PermissionDeniedByPolicy(dbus.DBusException):
    _dbus_error_name = "com.ubuntu.DeviceDriver.PermissionDeniedByPolicy"

class DBLockError(dbus.DBusException):
    _dbus_error_name = "com.quantumos.QSCDInterface.DBLockError"

class _DBLockError(Exception):
    pass
