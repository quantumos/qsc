import setuptools
import shutil

class DBusInstall(setuptools.Command):
    """
    Install DBus & polkit service files.
    """
    description = "Install DBus & polkit service files."
    user_options = [("files", None, "override service files to install")]

    def initialize_options(self):
        self.data_files=[("/usr/lib/systemd/system", ["qscd.service"]),
                    ("/usr/share/polkit-1/actions", ["com.quantumos.qscdservice.policy"]),
                    ("/etc/dbus-1/system.d", ["com.quantumos.QSCDService.conf"])],

    def finalize_options(self):
        pass

    def run(self):
        for dest, src in self.data_files[0]:
            for file in src:
                shutil.copy(file, dest)
                print("Installed {} to {}".format(file, dest))


setuptools.setup(
    name="qscd",
    version="0.1",
    author="Vincent Wang",
    cmdclass={
        "install_dbus_files": DBusInstall
    },
    packages=setuptools.find_packages()
)
